
nextflow.enable.dsl = 2

// custom sigmtx
process DECONVRUNGS {
    tag "CIBXgs"
    publishDir "${params.outdir}/CIBXres/GS"
    executor = 'local'
    cache 'lenient'

    input:
    tuple path(sampdata), path(pbmcmix), path(obserexpr), path(flowfract)
    path foofunr

    output:
    tuple path('refexpr4CIBXgsClass.txt'), path('refexpr4CIBXgs.txt'), emit: gsinput
    path('*.bm.K999.txt'), emit: gs
    tuple path('*.bm.K999.pdf'), path('*sourceGEP.txt'), emit: others 

    script:

    """
    deconvGS.R ${obserexpr} ${sampdata} ${foofunr}

    """

}

// deconv using custom
process DECONVRUN {
    tag "CIBXdeconv"
    publishDir "${params.outdir}/CIBXres/deconv"
    cache 'lenient'
    label 'process_8himem'

    input:
    tuple path(sampdata), path(pbmcmix), path(obserexpr), path(flowfract)
    path foofunin
    path sigmtx

    output:
    tuple path('*CD4*Window*.txt'), path('*CD8*Window*.txt'), path('*CD14*Window*.txt'), path('*CD19*Window*.txt'), emit: deconvres
    path '*Fractions.txt', emit: deconvfraction
    path '*GEPs*', emit: geps
    path '*Weights.txt', emit: weight
    path '*.png', emit: pngs
    path 'sigMTX4dev.txt', emit: sigmtx4dev

    script:

    """
    deconvrun.R ${sigmtx} ${pbmcmix} ${foofunin}

    """

}

// deconv using inbuilt
process LM22DECONVRUN {
    tag "CIBXLM22deconv"
    publishDir "${params.outdir}/CIBXres/LM22deconv"
    //executor = 'local'
    cache 'lenient'
    label 'process_8himem'

    input:
    tuple path(sampdata), path(pbmcmix), path(obserexpr), path(flowfract)
    path foofunr
    path sigmtx
    path LM22mergedclass
    path hgnctbl

    output:
    tuple path('*CD4*Window*.txt'), path('*CD8*Window*.txt'), path('*CD14*Window*.txt'), path('*CD19*Window*.txt'), emit: lm22deconvres
    path '*Fractions-Adjusted.txt', emit: lm22fraction
    path 'sigMTX4dev.txt', emit: lm22mtx


    script:

    """

    lm22deconvrun.R ${sigmtx} ${pbmcmix} ${foofunr} ${LM22mergedclass} ${hgnctbl}

    """

}



workflow cibxrun {

    take:
    deconvInput
    foofun
    lm22sigmtx
    lm22merged
    hgnctbl
    
    main:
    // CIBX generating custom
    DECONVRUNGS(
	deconvInput,
	foofun)

    //CIBX deconv using custom
    DECONVRUN(
	deconvInput,
	foofun,
	DECONVRUNGS.out.gs)

    //CIBX deconv using inbuilt
    LM22DECONVRUN(
	deconvInput,
	foofun,
	lm22sigmtx,
	lm22merged,
	hgnctbl)

    emit:
    customsigmtx = DECONVRUNGS.out.gs

}


 


