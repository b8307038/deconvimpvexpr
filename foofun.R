

calc_tpm <- function(x, gene.length) {
    x <- as.matrix(x)
    len.norm.lib.size <- colSums(x / gene.length)
    return((t(t(x) / len.norm.lib.size) * 1e06) / gene.length)
}

## tpm matrix for cibersort inputs
CIBXtpm_prep <- function(sampsub, countmtx, geneinfo, grp=NULL){
    require(edgeR)
    ## count matrix
    stopifnot((sampsub %in% colnames(countmtx)) %>% all)
    stopifnot(all(geneinfo$Geneid == rownames(countmtx)))
    objdsn <- DGEList(counts= countmtx[, sampsub],
                      genes = geneinfo[match(rownames(countmtx),
                                             geneinfo$Geneid), ]
                      )
    
    keep <- filterByExpr(objdsn, group = grp)
    objdsn <- objdsn[keep, , keep.lib.sizes=FALSE]
    
    objdsn <- calcNormFactors(objdsn)
    objdsn.expr <- calc_tpm(objdsn, objdsn$genes$Length)
    objdsn.expr
}


## input for generating gene signatures
CIBXgsinput_prep <- function(exprDGElist, refsampsub, sampmeta){
    require(data.table)
    stopifnot(all(sampmeta$sample %in% colnames(exprDGElist)))
    stopifnot( (refsampsub %in% sampmeta$samp) %>% all)
    ## cibersort pheno class
    refphenoset <- sampmeta[samp %in% refsampsub & celltype!="PBMC", ]
    refphenoset[, V3:=1]
    phenoclass <- dcast(refphenoset, celltype~sample, value.var = "V3")
    phenoclass[is.na(phenoclass)] <- 2

    ## sorted expression
    colneeded  <- c("Geneid", names(phenoclass)[-1])
    countcpmref <- exprDGElist[, names(phenoclass)[-1]] %>% as.data.frame
    countcpmref$genes <- rownames(countcpmref)
    countcpmref <- countcpmref[, c("genes", names(phenoclass)[-1])]
    stopifnot(names(countcpmref)[-1] %>% str_extract(., "CD14|CD19|CD4|CD8") == sampmeta$celltype[match(names(countcpmref)[-1], sampmeta$sample)]
              )

    list(gsinput = countcpmref, phenoclass = phenoclass)

}


cibxgs <- file.path("/rds/project/rds-csoP2nj6Y6Y/PEAC/CIBERSORTx", "hires.sif /src/CIBERSORTxFractions")
cibxtoken <- ""

## run signature module and return signature names and sig mtx
run_CIBX_gs <- function(output_dir = NULL, refexpr, refclass, remake=FALSE){

    require(data.table)
    
    if (!is.null(output_dir)) {

        output_dir <- file.path(getwd(), output_dir)
        
        if (!dir.exists(output_dir)) {
            dir.create(output_dir, recursive = TRUE)
        }
        
    } else {

        output_dir <- getwd()
    }
   
    ref_file <- "refexpr4CIBXgs.txt"
    class_file <- "refexpr4CIBXgsClass.txt"
    
    fwrite(refexpr,
           file = file.path(output_dir, ref_file),
           quote = FALSE,
           col.names = TRUE, row.names = FALSE, sep = "\t"
           )

    fwrite(refclass,
           file = file.path(output_dir, class_file),
           quote = FALSE,
           col.names = FALSE, row.names = FALSE, sep = "\t"
           )
        
    ## cibersortx signature module
    cibersortx_cmd <- paste0("singularity exec --bind ",
                             output_dir,
                             ":/src/data --bind ",
                             output_dir,
                             ":/src/outdir ",
                             cibxgs, 
                             cibxtoken,
                             " --refsample ", ref_file,
                             " --phenoclasses ", class_file,
                             " --G.min 300 --G.max 500 --q.value 0.01 --QN FALSE --single_cell FALSE"
                             )

    if (remake){

        cibersortx_cmd <- paste0(cibersortx_cmd, " --remake TRUE")

    }
    
    system(cibersortx_cmd)

    sigmtxfile <- paste0("CIBERSORTx_",
                         sub("\\.txt", "", class_file),
                         ".CIBERSORTx_",
                         sub("\\.txt", "", ref_file),
                         ".bm.K999.txt"
                         )
    ## read in as character to preserve the decimal numbers to be same as original one
    sigmtx <- fread(file.path(output_dir, sigmtxfile),
                    colClasses = "character"
                    )

    list(sigmtxfile = sigmtxfile, sigmtx = sigmtx)

}


#################
## CIBX deconv ##
#################

cibxdeconv <- file.path("/rds/project/rds-csoP2nj6Y6Y/PEAC/CIBERSORTx", "hires.sif /src/CIBERSORTxHiRes")

run_CIBX_deconv <- function(output_dir = NULL, sigmtx, pbmcmix, LM22=FALSE, LM22mergedclass=NULL){

    require(data.table)
    require(magrittr)

    if (!is.null(output_dir)) {

        output_dir <- file.path(getwd(), output_dir)
        
        if (!dir.exists(output_dir)) {
            dir.create(output_dir, recursive = TRUE)
        }
        
    } else {

        output_dir <- getwd()
    }

    sigmatxfile <- "sigMTX4dev.txt"
    PBMCmix_file <- "PBMCmix4deconv.txt"

    write.table(sigmtx,
                file = file.path(output_dir, sigmatxfile),
                quote = FALSE,
                col.names = TRUE, row.names = FALSE, sep = "\t"
                )

    write.table(pbmcmix,
                file = file.path(output_dir, PBMCmix_file),
                quote = FALSE,
                col.names = TRUE, row.names = FALSE, sep = "\t"
                )
    

    numCores <- ifelse(Sys.getenv("SLURM_JOB_CPUS_PER_NODE")=="",
                       4,
                       as.numeric(Sys.getenv("SLURM_JOB_CPUS_PER_NODE"))
                       )
    
    cibersortx_cmd <- paste0("singularity exec --bind ",
                             output_dir,
                             ":/src/data --bind ",
                             output_dir,
                             ":/src/outdir ",
                             cibxdeconv,
                             cibxtoken,
                             " --sigmatrix ", sigmatxfile,
                             " --mixture ", PBMCmix_file,
                             " --variableonly TRUE",
                             " --QN FALSE --threads ",
                             numCores
                             )

    if (LM22){

        mergedclass <- "mergedclasses.txt"

        cat(LM22mergedclass,
            file = file.path(output_dir, mergedclass),
            sep="\t")
        
        cibersortx_cmd <- paste0(cibersortx_cmd,
                                 " --classes ", mergedclass,
                                 " --rmbatchBmode TRUE"
                                 )
        
    }
    
    system(cibersortx_cmd)

}


