
nextflow.enable.dsl = 2

process PREPCHUNK {
    tag "${celltype}_PenalisedInput"
    label 'process_himem'
    publishDir "${params.outdir}/PenalisedInput"

    input:
    tuple path(sampdata), path(pbmcmix), path(obserexpr), path(flowfract)
    val chunk
    each celltype

    output:
    path '*chunk*.rds', emit: penrdsch

    script:

    """
    prep4penalised.R ${sampdata} ${obserexpr} ${celltype} ${chunk}
    
    """
}

process PENALRUN {
    tag "${runid}"
    label 'process_glmnet'
    publishDir "${params.outdir}/PenalisedRes/${method}"


    input:
    path rds
    each method
    

    output:
    path '*.rds', emit: allres

    script:
    runid = "${rds.simpleName}${method}"

    """
    penaliseRun.R ${rds} ${method} $task.cpus

    """
}


workflow runPenalised {

    take:
    deconvInput
    chunksize
    celltypein
    appr

    main:
    // expr by chunk
    PREPCHUNK(
	deconvInput,
	chunksize,
	celltypein
    )

    // glmnet models
    PENALRUN(
	PREPCHUNK.out.penrdsch.flatten(),
	appr
    )

}
