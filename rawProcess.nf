
nextflow.enable.dsl = 2

// read count and gene meta
process CTRDSPREP {
    publishDir "${params.outdir}/processedData"
    executor = 'local'

    input:
    val ctbl

    output:
    tuple path('*countdsn.rds'), path('*geneMeta.rds'), emit: ctmtx 

    script:

    """
    countmtx.R "${ctbl}"

    """
}

// sample catalogue
// process SAMPCATAPREP {
//     publishDir "${params.outdir}/processedData"
//     executor = 'local'

//     input:
//     path sampcata
    
//     output:
//     path '*Samplecatalogue.rds', emit: sampcata

//     script:

//     """

//     samplecatalogue.R ${sampcata}
    
//     """

// }

// batch-corrected read counts
process ADJCOUNT {
    tag "combatseq"
    label 'process_1low'
    publishDir "${params.outdir}/processedData"

    input:
    tuple path(ctmtx), path(genemeta)
    path sampcata
    
    output:
    tuple path('*adjcountdsn.rds'), path('*adjgeneMeta.rds'), emit: adjctmtx


    script:

    """
    
    adjcountmtx.R ${ctmtx} ${genemeta} ${sampcata}
    
    """

}

// inputs for deconv run
process DECONVINPUT {
    publishDir "${params.outdir}/deconvInput"
    executor = 'local'

    input:
    tuple path(ctmtx), path(genemeta)
    path sampcata
    path sortreport
    path foofun

    output:
    tuple path('sampdata.txt'), path('PBMCmix4deconv.txt'), path('ObservTPMExpr.rds'),  path('flowFract_wide.rds'), emit: deconvInput

    script:

    """
    deconvInput.R ${ctmtx} ${genemeta} ${sampcata} ${sortreport} ${foofun}

    """
}


workflow runRawProcess {

    take:
    ctbl
    samplecata
    sortreport
    foofun
    
    main:

    CTRDSPREP(ctbl)
    
    ADJCOUNT(CTRDSPREP.out.ctmtx,
	     samplecata)

    // inputs for deconv
    DECONVINPUT(
	ADJCOUNT.out.adjctmtx,
	samplecata,
	sortreport,
	foofun
    )

    emit:
    deruninput = DECONVINPUT.out.deconvInput
    
    
}

