---
title: Additional figures for reviewers/supplmentary
date-modified: last-modified
date-format: "DD-MMM-YY"
lightbox: true
format:
  html:
    code-fold: true
    toc: true
    page-layout: full
    number-sections: true
---



```{r, fig.show = 'hide', echo=FALSE, message=FALSE, warning = FALSE}
rm(list = ls())
kpkgs <- c("data.table", "magrittr", "stringr", "knitr", "styler",
           "ggplot2", "RColorBrewer", "ggrepel",
           "gridExtra", "edgeR", "ggpubr", "cols4all", "kableExtra")

invisible(lapply(kpkgs, require, character.only = TRUE))
suppressPackageStartupMessages(library(ComplexHeatmap))
library(foreach)
library(doParallel)


knitr::opts_chunk$set(collapse = TRUE,
                      echo = FALSE,
                      warning = FALSE,
                      message = FALSE,
                      cache = TRUE,
                      cache.lazy = FALSE,
                      out.width = "100%",
                      fig.width = 8,
                      fig.height = 6,
                      fig.align = "center",
                      tidy = "styler",
                      fig.show = "hold",
                      fig.wide =  TRUE,
                      comment = "")

if (!knitr::is_latex_output()) {

    knitr::opts_chunk$set(dev = "png",
                          dev.args = list(type = "cairo-png"),
                          echo = TRUE,
                          collapse = TRUE)

} else {

    knitr::opts_chunk$set(dev = "cairo_pdf",
                          results = "hide")

}

## get ride of lapply notation
## https://stackoverflow.com/questions/54300147/suppress-list-notation-in-rmarkdown-from-lapply
def <- knitr::knit_hooks$get("output")
knitr::knit_hooks$set(output = function(x, options) {
  x <- def(x, options)
  ifelse(!is.null(options$suppress), gsub(pattern = "```.*```", "", x), x)
})

source("./Rmdfoorun.R")

expr_order <- c("inbuilt", "custom", "bMIND", "swCAM", "LASSO", "RIDGE")
appro.cols <- cols4all::c4a("okabe", n = length(expr_order))
appro.cols[4] <- c4a("okabe", n = 7)[7]
names(appro.cols) <- expr_order

## sample catalogue
sampdata <- fread("./NextflowRes/deconvInput/sampdata.txt")
sampdata <- fread("./DataDryad/Sample_annotated_Datadryad.csv")
##
sctorder <- c("CD4", "CD8",  "CD14", "CD19")

## color setting
## fract cell types
ctfrat.pal <- cols4all::c4a("rainbow", n = length(sctorder))
names(ctfrat.pal) <- sctorder

```

# CLUSTER Data-other comments


```{r}
#| eval: !expr '!interactive()'

## examine if sex differ between train/test sets
sapply(sctorder, function(ctin) {
    sampdata[celltype == ctin, table(sex, samtype)] %>%
        chisq.test %>%
        .[c("statistic", "parameter", "p.value")]
}) %>% t %>%
    set_colnames(c("X-squared", "df", "p.value")) %>%
    as.data.frame() %>%
    kbl(x = ., digits = 2, caption = "Chi-squared tests for associations betwen sex and train/test sets per celltype") %>%
    kable_paper(full_width = FALSE) %>%
    column_spec(4, bold = T)

```


```{r fig3expr}

## observed expression
## read in imputation exprssion
## log2(TPM+1) for CIBX, swCAM
## expr_scenarios: imp expr for testing samples
## list of scenarios; with each scenarios, list of cell types
source("Fig3expr_data.R")

## extra copy for imp expr
expr_ori <- copy(expr_scenarios)

## common genes per cell type
gcommon <- lapply(sctorder, function(ctin) {
    lapply(expr_scenarios, "[[", ctin) %>%
        lapply(., rownames) %>%
        Reduce(f = intersect, x = .)
})
names(gcommon) <- sctorder

sapply(gcommon, length)

## imputed expression limited to the common genes
expr_scenarios <- lapply(expr_scenarios, function(sce) {

    res <- lapply(sctorder, function(ctin) {
        sce[[ctin]][gcommon[[ctin]], ]
    })
    names(res) <- sctorder
    res
})

names(appro.cols) <- expr_order


## trainsamples
train_samps_ct <- split(
    sampdata[samtype == "refsamp" & celltype != "PBMC", sample],
    sampdata[samtype == "refsamp" & celltype != "PBMC", celltype]
)

## trainssample
train_samps_ct <- lapply(train_samps_ct, function(xin) {
    trainssamp <- sampdata[match(xin, sample), samp]
    trainspbmc <- sampdata[celltype == "PBMC", ][match(trainssamp, samp), sample]
    names(xin) <- trainspbmc
    xin
})

train_samps_ct <- train_samps_ct[sctorder]


```

# CLUSTER Data-Figure3


* Figure 3 in our manuscript shows the prediction accuracy of cell-type expression on **test** samples based on imputed **gene sets that vary with approaches**.

* Here, we show the results of Figure 3 based on the **common gene sets** across approaches. No. common genes are `r sprintf("%d for %s", sapply(gcommon, length), names(gcommon)) %>% combine_words(before = "**", after = "**")`, respectively.



{{< include commongenesetFig3.qmd >}}


# CLUSTER Data-Figure4

* We simulated 10 dichotomous phenotypes using PCA analysis based on observed cell-type exprssion on train and test samples (`Train + Test samples`).

* For each phenotype, we carried out the DGE analysis for both [1] & [2] expression matrix. We used FDR of 0.05 in DGE results from [1] as groud truth and varyied FDR from 0 to 1 by 0.05 to make decisions on DGE results from [2]. We then ran the ROC analysis based on the classification contingency tables to calculate AUC. 

* Here, we expanded the simulated phenotypes and considered sex as a covariate for DGE analysis.

  1. dichPheno: simulated dichotomous phenotype
  
  2. dichPheno+sex: simulated dichotomous phenotype and sex
  
  3. contPheno: continuous phenotype
  
  4. contPheno+sex: continuous phenotype and sex


* These four scenarios were evaluated in `different gene sets` (no. predicted genes vary with approaches) and  `common gene sets` (common genes across approaches) for both `Train + Test samples` and `Test samples`. 


## Train + Test samples

* observed expr on train + test samples (if they have cell-type data) were used to simulate the phenotypes using PCA

  + No. train samples are 80 per celltype
  + No. test samples are `r sprintf("%d for %s", sapply(expr_scenarios[[2]], ncol), names(expr_scenarios[[2]])) %>% combine_words()`, respectively.

* [1] observed expr on train + test samples were used in DGE based on simulated phenotypes from the previous steps.

* [2] observed expr on train + imputed expr on test samples had observed data were used for DGE.

* [1] & [2] have the exact dimensions. Test samples without cell-type data were not assigned to any simulated phenotypes, so such test samples were excluded from DGE analysis.


{{< include traintestFig4.qmd >}}


## Test samples

* observed expr on test samples (if they have cell-type data) were used to simulate the phenotypes using PCA

  + No. test samples are `r sprintf("%d for %s", sapply(expr_scenarios[[2]], ncol), names(expr_scenarios[[2]])) %>% combine_words()`, respectively.

* [1] observed expr on test samples were used in DGE based on simulated phenotypes from the previous steps.

* [2] Imputed expr on test samples who had observed data were used for DGE.

* [1] & [2] have the exact dimensions. Test samples without cell-type data were not assigned to any simulated phenotypes, so such test samples were excluded from DGE analysis.



{{< include testsetonlyFig4.qmd >}}


## Train + Test samples [1] & Test samples [2]

* Observed expr on train + test samples (if they have cell-type data) were used to simulate the phenotypes using PCA

  + No. train samples are 80 per celltype
  + No. test samples are `r sprintf("%d for %s", sapply(expr_scenarios[[2]], ncol), names(expr_scenarios[[2]])) %>% combine_words()`, respectively.

* [1] Observed expr on **train + test** samples were used in DGE based on simulated phenotypes from the previous steps.

* [2] Imputed expr on **test** samples who had observed data were used for DGE.

* [1] & [2] **DO NOT** have the exact dimensions in DGE analysis. Test samples without cell-type data were not assigned to any simulated phenotypes, so such test samples were excluded from DGE analysis.


{{< include testsetonly2Fig4.qmd >}}

