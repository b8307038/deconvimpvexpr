
nextflow.enable.dsl = 2

// debCAM fraction
process DEBCAMFRACTS80 {
    tag "debCAMfracts80"
    publishDir "${params.outdir}/swCAMres"
    executor = 'local'

    input:
    tuple path(sampdata), path(pbmcmix), path(obserexpr), path(flowfract)

    output:
    path 'debCAM_FractresPure80.rds', emit: debCAMfract80

    script:
    
    """
    debCAMFractSknow.R ${pbmcmix} ${flowfract} ${obserexpr} ${sampdata}
    
    """
}

// swCAM cv
process SWCAMCV {

    tag "swCAMcv"
    label 'process_swcamcv'
    publishDir "${params.outdir}/swCAMres"
    

    input:
    tuple path(sampdata), path(pbmcmix), path(obserexpr), path(flowfract)
    path swcamcvr

    output:
    path 'cross-validation.RData', emit: swCAMcv
    path 'cross-validation.png', emit: swCAMcvpng

    script:
    """
    script-swCAM-cv.R ${pbmcmix} ${flowfract} ${swcamcvr}
    
    """
}

// swCAM cell expr
process SWCAMRUN {
    tag "swCAMrun"
    label 'process_swcam'
    publishDir "${params.outdir}/swCAMres"
 
    input:
    path rdata
    path swcamr

    output:
    path 'sample-wiseS.RData', emit: swCAMres

    script:
    """
    script-swCAM.R ${rdata} ${swcamr}
    
    """
}


workflow runSWCAM {

    take:
    data4deconv
    swcamcvr
    swcamr

    main:
    
    // debCAM fraction
    DEBCAMFRACTS80(
	data4deconv
    )
    // swCAM sample-wise cell type expr
    SWCAMCV(
	data4deconv,
	swcamcvr
    )

    SWCAMRUN(
	SWCAMCV.out.swCAMcv,
	swcamr)
 
}

