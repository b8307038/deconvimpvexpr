#!/usr/bin/env Rscript

rm(list = ls())

args=(commandArgs(TRUE))


library("data.table")
library("magrittr")

bulkin <- args[1]
## TPM
## bulkin <- fread("./NextflowRes/deconvInput/PBMCmix4deconv.txt")
bulkin <- fread(bulkin)

dim(bulkin)

bulk <- bulkin[, -1]
setDF(bulk)
rownames(bulk) <- bulkin$GeneSymbol
bulk <- log2(bulk+1)

dim(bulk)


## flowfract <- readRDS("./NextflowRes/deconvInput/flowFract_wide.rds")
flowfract <- readRDS(args[2])

bulk <- bulk[, rownames(flowfract)]

stopifnot(names(bulk) == rownames(flowfract))

## custom signature
## oursigmtx <- fread("./NextflowRes/CIBXres/GS/CIBERSORTx_refexpr4CIBXgsClass.CIBERSORTx_refexpr4CIBXgs.bm.K999.txt")
args[3]
oursigmtx <- fread(args[3])

sortedmtx <- oursigmtx[, -1] %>% as.matrix
rownames(sortedmtx) <- oursigmtx$NAME
sortedmtx <- sortedmtx[, colnames(flowfract)]
sortedmtx <- log2(sortedmtx + 1)


###########
## bMIND ##
###########
library(MIND)

set.seed(111)

deconv3 = bMIND(bulk = bulk, signature = sortedmtx, frac_method = "NNLS")

saveRDS(deconv3, "bMIND_Fractres.rds")


