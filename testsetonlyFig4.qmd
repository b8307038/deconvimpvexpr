

```{r fig4rundatatest, cache = TRUE}

## main: different gene set; common: same genes per cell across methods
impvexpr_all <- list(main = expr_ori, common = expr_scenarios)

## TRUE for dichotomous; FALSE for continuous
dcht_pheno <- c(TRUE, FALSE)
names(dcht_pheno) <- c("dcht", "cntn")

numCores <- length(expr_order)
numCores

cl <- makeCluster(numCores, type = "FORK")
registerDoParallel(cl)

aucall <- foreach(j = seq_along(impvexpr_all),
                  .final = function(x) setNames(x, names(impvexpr_all))) %:%
    foreach(
        i = dcht_pheno,
        .final = function(x) setNames(x, names(dcht_pheno))
    ) %dopar% {

        dat <- run_datprepfig4(obvexpr = observy,
                               impvexpr = impvexpr_all[[j]],
                               tsampct = NULL,
                               dich = i)
        ## expr ~ pheno
        res1 <- run_fig4(datprlst = dat, truesig = 0.05)

        ## expr~ sex + pheno
        res2 <- run_fig4(
            datprlst = dat, truesig = 0.05,
            covdata = sampdata, covid = "sample", covar = "sex")

        list(nocov = res1, covin = res2)

   }

stopCluster(cl)


```


### Varying gene sets


```{r}
#| fig-cap: !expr 'paste0("Differential gene expression (DGE) recovery. Area under curve (AUC) distributions by cell type (columns) and approach for each scenario (rows). dichPheno: simulated dichotomous phenotype;  dichPheno+sex: simulated dichotomous phenotype and sex; contPheno: continous phenotype; contPheno+sex: continous phenotype and sex. Each point is a simulated phenotype, and ten phenotypes were simulated. For each simulated phenotype, the receiver operating characteristic curve and AUC were estimated by FDR fixed at 0.05 in the observed data and varied FDRs from 0 to 1 by 0.05 in the imputed data. Box plots showed the AUC distributions, with horizontal lines from the bottom to the top for 25 %, 50 % and 75 % quantiles, respectively. inbuilt: CIBERSORTx with the inbuilt signature matrix; custom: CIBERSORTx with a custom signature matrix; bMIND: bMIND with flow fractions; swCAM: swCAM with flow fractions; LASSO/RIDGE: regularised multi-response Gaussian models. ", ifelse(nrow(achk!=0), achk_words, ""))'
#| label: fig-aucpltmaintest
#| fig-width: 12.5
#| fig-height: 15

mychk <- unlist(aucall, recursive = FALSE) %>%
    unlist(., recursive = FALSE) %>%
    lapply(., "[[", "aucres") %>%
    lapply(., function(datin) {
        datin[, ':=' (
            celltype = factor(celltype, levels = sctorder),
            approach = factor(approach, levels = expr_order))]
        datin
    }) %>% rbindlist(l = ., idcol = "scenarios") %>%
    .[, pheno := gsub("^main\\.|^common\\.", "", scenarios)] %>%
    .[, scenario := str_extract(scenarios, "main|common")]

pheno.order <- CJ(names(dcht_pheno), c("nocov", "covin"), sorted =  FALSE)[, paste(V1, V2, sep = ".")]
pheno.order
mychk[, pheno := factor(pheno, levels = pheno.order, labels = c("dichPheno", "dichPheno+sex", "contPheno", "contPheno+sex"))]

## no. auc available
achk <- mychk[scenario == "main" & !is.na(auc),  .N, by = c("pheno", "celltype", "approach")][N != 10, ]
achk

achk_words <- "**Noted AUC was not calculated for some simulated phenotypes because all associations were true negative, and none were false negative. Scenario-celltype-approach with no. AUCs $<$ 10 are listed in the code chunk.**"


ggboxplot(data = mychk[scenario == "main", ],
          x = "approach", y = "auc", ylab = "AUC",
          palette = appro.cols,
          alpha = 0.35, fill = "approach",
          add = c("jitter"),
          add.params = list(shape = 21, size = 2.5, alpha = 1)) +
    facet_grid(pheno ~ celltype, scales = "free") +
    labs(fill = NULL) +
    used_ggthemes(axis.text.x = element_blank(),
                  legend.position = "top",
                  strip.text.y.right = element_text(angle = 0),
                  legend.text = element_text(size = rel(1))) +
    rremove("xlab")

```


```{r}
#| label: tbl-tblsummaintest

mychk_tbl <- copy(mychk)
mychk_tbl[, .N, by = c("scenarios", "celltype", "approach")]

tbl_summary <- mychk_tbl[, list(
    Q50 = median(auc, na.rm = TRUE) %>% round(., 2),
    Q25 = quantile(auc, probs = 0.25, na.rm = TRUE) %>% round(., 2),
    Q75 = quantile(auc, probs = 0.75, na.rm = TRUE) %>% round(., 2)
)
, by = c("scenarios", "celltype", "approach")][
, V1 := paste0(Q50, " (", Q25, "-", Q75, ")")]

tbl_summary[, pheno := gsub("^main\\.|^common\\.", "", scenarios)]
tbl_summary[, scenario := str_extract(scenarios, "main|common")]

tbl_res <- dcast(data = tbl_summary, pheno+celltype ~ scenario + approach,
                 value.var = "V1")

setDF(tbl_res, rownames = paste0(tbl_res$pheno, tbl_res$celltype))

pheno.order <- CJ(names(dcht_pheno), c("nocov", "covin"), sorted =  FALSE)[, paste(V1, V2, sep = ".")]

cols.order <- CJ(c("main", "common"), V2 = expr_order, sorted = FALSE)[, paste(V1, V2, sep = "_")]

rows.order <- CJ(pheno.order, sctorder, sorted = FALSE)[, paste0(pheno.order, sctorder)]

tbl_out <- tbl_res[rows.order, c("celltype", grep("main", cols.order, value = TRUE))]
names(tbl_out) <- gsub("main_|common_", "", names(tbl_out))

capin <- paste0("Median AUC (Q25-Q75) across",
                ifelse(nrow(achk) != 0, "", uniqueN(mychk$fakedPCs)),
                " simulated phentoypes per cell by apparoch. **Genes common across approaches per cell type**")


kbl(x = tbl_out, row.names = FALSE,
    caption = capin) %>%
    kable_paper("striped", full_width = FALSE) %>%
    pack_rows("dichotomous pheno", 1, 4,
              label_row_css = "text-align: left;") %>%
    pack_rows("dichotomous pheno + sex", 5, 8) %>%
    pack_rows("continous pheno", 9, 12) %>%
    pack_rows("continous pheno + sex", 13, 16) %>%
    column_spec(column = 6:7, bold = T)

```

### Common gene sets




```{r}
#| fig-cap: !expr 'paste("Differential gene expression (DGE) recovery. **Genes common across approaches were used**. No. common genes are ", sprintf("%d for %s", sapply(gcommon, length), names(gcommon)) %>% combine_words, "Area under curve (AUC) distributions by cell type (columns) and approach for each scenario (rows). **dichPheno: simulated dichotomous phenotype;  dichPheno+sex: simulated dichotomous phenotype and sex; contPheno: continous phenotype; contPheno+sex: continous phenotype and sex**. Each point is a simulated phenotype, and ten phenotypes were simulated. For each simulated phenotype, the receiver operating characteristic curve and AUC were estimated by FDR fixed at 0.05 in the observed data and varied FDRs from 0 to 1 by 0.05 in the imputed data. Box plots showed the AUC distributions, with horizontal lines from the bottom to the top for 25 %, 50 % and 75 % quantiles, respectively. inbuilt: CIBERSORTx with the inbuilt signature matrix; custom: CIBERSORTx with a custom signature matrix; bMIND: bMIND with flow fractions; swCAM: swCAM with flow fractions; LASSO/RIDGE: regularised multi-response Gaussian models. ", ifelse(nrow(achk1!=0), achk_words, ""))'
#| label: fig-aucpltcommontest
#| fig-width: 12.5
#| fig-height: 15

achk1 <- mychk[scenario == "common" & !is.na(auc),  .N, by = c("pheno", "celltype", "approach")][N != 10, ]
achk1

ggboxplot(data = mychk[scenario == "common", ],
          x = "approach", y = "auc", ylab = "AUC",
          palette = appro.cols,
          alpha = 0.35, fill = "approach",
          add = c("jitter"),
          add.params = list(shape = 21, size = 2.5, alpha = 1)) +
    facet_grid(pheno ~ celltype, scales = "free") +
    labs(fill = NULL) +
    used_ggthemes(axis.text.x = element_blank(),
                  legend.position = "top",
                  strip.text.y.right = element_text(angle = 0),
                  legend.text = element_text(size = rel(1))) +
    rremove("xlab")


```


```{r}
#| label: tbl-tblsumcommontest

tbl_out <- tbl_res[rows.order, c("celltype", grep("common", cols.order, value = TRUE))]
names(tbl_out) <- gsub("main_|common_", "", names(tbl_out))

capin <- paste0("Median AUC (Q25-Q75) across",
                ifelse(nrow(achk1) != 0, "", uniqueN(mychk$fakedPCs)),
                " simulated phentoypes per cell by apparoch. **Genes common across approaches per cell type**")

kbl(x = tbl_out, row.names = FALSE,
    caption = capin) %>%
    kable_paper("striped", full_width = FALSE) %>%
    column_spec(column = 6:7, bold = TRUE) %>%
    pack_rows("dichotomous pheno", 1, 4,
              label_row_css = "text-align: left;") %>%
    pack_rows("dichotomous pheno + sex", 5, 8) %>%
    pack_rows("continous pheno", 9, 12) %>%
    pack_rows("continous pheno + sex", 13, 16)

```
