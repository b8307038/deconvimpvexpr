
# Deconvolution and Penalised regressions for cell type expression

- this workflow is used for "Penalised regression improves imputation of cell-type specific expression using RNA-seq data from mixed cell populations compared to domain-specific methods"


## Workflow structure

- It is built using [Nextflow](https://www.nextflow.io/)  

- ``main.nf`` is the main configuration, controlling how data flow into different processes/jobs.  
  
- ``main.nf`` comprises 5 sub-workflows. Each sub-workflow has several processes, each with a Rscript stored in the ``bin`` folder. These sub-workflows are detailed below. 

1. ``rawProcess.nf``
---

| process     | Rscript       | what it does                         |
|-------------|---------------|--------------------------------------|
| CTRDSPREP   | countmtx.R    | assemble read counts                 |
| ADJCOUNT    | adjcountmtx.R | generate batch-corrected read counts |
| DECONVINPUT | deconvInput.R | prepare deconvolution inputs         |


2. ``cibxrun.nf``
---

| process       | Rscript         | what it does                           |
|---------------|-----------------|----------------------------------------|
| DECONVRUNGS   | deconvGS.R      | CIBERSORTx for signature matrix        |
| DECONVRUN     | deconvrun.R     | CIBERSORTx deconvoltuion using custom  |
| LM22DECONVRUN | lm22deconvrun.R | CIBERSORTx deconvoltuion using inbuilt |

3. ``bMINDrun.nf``
---

| process    | Rscript      | what it does                   |
|------------|--------------|--------------------------------|
| BMINDFRACT | bMINDfract.R | bMIND fraction deconvolution   |
| BMIND      | bMINDrun.R   | bMIND expression deconvolution |


4. ``swCAMrun.nf``
---

| process        | Rscript            | what it does                           |
|----------------|--------------------|----------------------------------------|
| DEBCAMFRACTS80 | debCAMFractSknow.R | debCAM fraction deonvolution           |
| SWCAMCV        | script-swCAM-cv.R  | swCAM finding lambda for deconvolution |
| SWCAMRUN       | script-swCAM.R     | swCAM expression deconvolution         |


5. ``penalisedImpvrun.nf``
---

| process   | Rscript          | what it does                        |
|:----------|:-----------------|:------------------------------------|
| PREPCHUNK | prep4penalised.R | dividing gene chunks for imputation |
| PENALRUN  | penaliseRun.R    | LASSO & RIDGE imputation            |

## R scripts and markdown files

- `foofun.R`: R functions used in this work
- `Rmdfoorun.R`: R functions used in AnalysisResultFigures.Rmd 
- `AnalysisFlowR1.Rnw`: Sweave Rnw for analysis flow figure
- `AnalysisResultFigures.Rmd`: data analysis and figures

## Execution report

- [Nextflow report](https://b8307038.gitlab.io/deconvimpvexpr/nextflow_report.html)

- workflow DAG

![workflow][logo]

[logo]: NextflowRes/workflowInfo/flowchart_dag.png "FlowChart"

## Data analysis

- [Analysis & Figures](https://b8307038.gitlab.io/deconvimpvexpr/AnalysisResultFigures.html)
