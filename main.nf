#! /usr/bin/env nextflow

nextflow.enable.dsl = 2

include { runRawProcess } from './rawProcess' params(params)

include { cibxrun } from './cibxrun' params(params)

include { runBMIND } from './bMINDrun' params(params)

include { runSWCAM } from './swCAMrun' params(params)

include { runPenalised } from './penalisedImpvrun' params(params)

// count tables
ctbl = file(params.ctbl, checkIfExists: true)
// sample catelogue
samplecata = file(params.sampcata, checkIfExists: true)
// R functions
foofun = file(params.foofun, checkIfExists: true)
// flow report
sortreport = file(params.sortreport, checkIfExists: true)


workflow {

    runRawProcess(
	ctbl,
	samplecata,
	sortreport,
	foofun
    )

    // cibx subworkflow using custom and lm22

    lm22sigmtx = file(params.LM22, checkIfExists: true)
    lm22merged = file(params.LM22mergedclass, checkIfExists: true)
    hgnctbl = file(params.hgnctbl, checkIfExists: true)

    cibxrun(
	runRawProcess.out.deruninput,
	foofun,
	lm22sigmtx,
	lm22merged,
	hgnctbl
    )

    // bMIND subflow

    runBMIND(
	runRawProcess.out.deruninput,
	cibxrun.out.customsigmtx
    )

    // swCAM subflow
    
    runSWCAM(
	runRawProcess.out.deruninput,
	params.swcamcvr,
	params.swcamr
    )


    // lasso and ridge
    cellin= ['CD4', 'CD8', 'CD14', 'CD19']
    appr = ['lasso', 'ridge']
    
    runPenalised(
	runRawProcess.out.deruninput,
	params.chunksize,
	cellin,
	appr
    )

}

