
nextflow.enable.dsl = 2

// bmind fract
process BMINDFRACT {
    tag "bmindrunfract"
    label 'process_himem'
    publishDir "${params.outdir}/bMINDres"
    

    input:
    tuple path(sampdata), path(pbmcmix), path(obserexpr), path(flowfract)
    path sigmtx

    output:
    path 'bMIND_Fractres.rds', emit: bmindfract

    script:
    """
    bMINDfract.R ${pbmcmix} ${flowfract} ${sigmtx}
    
    """
}

// bmind imp expr
process BMIND {
    tag "bmindrun"
    label 'process_8himem'
    publishDir "${params.outdir}/bMINDres"
    

    input:
    tuple path(sampdata), path(pbmcmix), path(obserexpr), path(flowfract)

    output:
    path 'bMIND_res.rds', emit: bmindres

    script:
    """
    bMINDrun.R ${pbmcmix} ${flowfract}
    
    """
}


workflow runBMIND {

    take:
    data4deconv
    customsigmtx

    main:

    BMINDFRACT(data4deconv, customsigmtx)

    BMIND(data4deconv)
 
}

